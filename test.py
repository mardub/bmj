from scrape_bmj import get_data
from scrape_bmj import get_filename
from colorama import Fore
import time

#time.sleep(3)
print("First test")
#assert get_data("2207032")["year"]==1890, Fore.RED +"Error not getting year properly"

#time.sleep(3)

#assert get_data("2207032")["day"]==4, Fore.RED +"Error not getting day properly"
time.sleep(3)
print("Second test: Article")
#assert get_filename(get_data("2207032"),"https://www.ncbi.nlm.nih.gov/pmc/issues/159430/")=="BMJ_1890_01_04_vol001_nr1514_art001",Fore.RED + "error not right file name"
#https://www.ncbi.nlm.nih.gov/pmc/resources/citations/159430/export/"

time.sleep(3)
print("third test: Supplement page")
#assert get_data("1787297")["issue"]=="Suppl", Fore.RED + "Error not getting issue properly"
#assert get_data("1785429")["
#time.sleep(60)
print("fourth test: Supplement page and article")
#assert get_filename(get_data("1787297"),"https://www.ncbi.nlm.nih.gov/pmc/issues/141830/")== "BMJ_1972_06_20_vol002_nrSuppl_art001",Fore.RED + "error not right file name"
time.sleep(3)
print("fifth test: Supplement page and Journal Matter")
#assert get_filename(get_data("1787300","https://www.ncbi.nlm.nih.gov/pmc/issues/141830/"),"https://www.ncbi.nlm.nih.gov/pmc/issues/141830/")=="BMJ_1972_06_20_vol002_nrSuppl_jm001",Fore.RED + "error not right file name"
time.sleep(10)
print("Sixth test: cover page")
assert get_filename(get_data("1700884","https://www.ncbi.nlm.nih.gov/pmc/issues/138636/",cover_page=True),"https://www.ncbi.nlm.nih.gov/pmc/issues/138636/")=="BMH_1983_07_09_vol287_nr6385_cp001",Fore.RED + "error not right file name"
#https://www.ncbi.nlm.nih.gov/pmc/issues/138636/

#https://www.ncbi.nlm.nih.gov/pmc/articles/PMC1787300/pdf/brmedj02210-0001.pdf
#https://www.ncbi.nlm.nih.gov/pmc/resources/citations/1785429/export/"
#https://www.ncbi.nlm.nih.gov/pmc/resources/citations/1548855/export/
#https://www.ncbi.nlm.nih.gov/pmc/resources/citations/1548854/export/
#https://www.ncbi.nlm.nih.gov/pmc/resources/citations/1787297/export/
#BMJ_1890_vol001_nr1514_art001__5646546
#BMJ_1890_vol001_nr1514_art-suppl
#BMJ_1890_vol001_nr1514_