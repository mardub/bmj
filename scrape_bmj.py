#Author: Marie Dubremetz @ Center for Digital Humanities Uppsala
#Program to download the PDF of the medical journals 
#from 1890 to 1990 (or whaterver start end date you decide)
import urllib.request, urllib.error, urllib.parse
from bs4 import BeautifulSoup
import time
import sys
import requests
from dateutil import parser
from random import randint
from colorama import Fore
import os

#0. Adapt the following variables to your specific case:
startDate = 1890
endDate = 1915
baseUrl = "https://www.ncbi.nlm.nih.gov/pmc/journals/3/"#Example for medical journal. adapt the baseURL accordingly
baseName = "BMJ_"#whaterver name for your output files
opener = urllib.request.build_opener()
headers = {'User-Agent': 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2229.0 Safari/537.36',}
opener.addheaders = [('User-agent', 'Mozilla/5.0')]






def clean_space(line):
    return line.replace(" ", "").replace("\n", "").lstrip().strip()

def write_temp_ref(pmc):
    ref_link = "https://www.ncbi.nlm.nih.gov/pmc/resources/citations/"+pmc+"/export/"
    ref = requests.get(ref_link)
    #save the reference file in "temp"
    with open("temp", 'wb') as f:#uncomment at each new test design
        f.write(ref.content)
    #if ref contains "PMC Site is overloaded."
    if "PMC Site is overloaded." in ref.text:
        print("PMC Site is overloaded.")
        time.sleep(70)
        headers = {'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/120.0.0.0 Safari/537.36 Edg/120.0.0.',}
        opener.addheaders = [('User-agent', 'Mozilla/5.0')]
        write_temp_ref(pmc)
    else:
        print("everything fine with the temp file!")
        
        
    return None

def get_data(pmc,parent_page=None,pdf_type="art",cover_page=False):
    """ from the pmc number, get the data of publication
    intermediate: a reference file that looks like this:
    'PMID- 20794590
    OWN - NLM
    STAT- PubMed-not-MEDLINE
    VI  - 13
    IP  - 3
    DP  - 1849 Feb 7
    PG  - 62-6
    [...]
    SO  - Prov Med Surg J. 1849 Feb 7;13(3):62-6. doi: 10.1136/bmj.s1-13.3.62.'
    returns the date of publication, the volume 
    >>> get_data("2207032")
    {'vol': '001', 'year': 1890, 'month': 1, 'day': 4, 'issue': ' 1514\n'}
    """
    #reopen ref_file
    print("pmc in get_data:"+pmc)
    write_temp_ref(pmc)
    ref_file = open("temp", "r")
    if "Sorry, the page you requested can't be found. Here are some suggestions" in ref_file.read():
        ref_file.close()
        #get from the parent page the position of the pdf that has the pmc
        #get the position of the link that has the pmc
        document = open("temp_parent",'r')
        soup = BeautifulSoup(document , "lxml")
        #get all the links that have for htext "PDF–" with soup
        links = soup.find_all("a")
        #get the position of the link that has the pmc
        for i, link in enumerate(links):
            if pmc in link['href']:
                position = str(i+1).zfill(3)
                print("position: "+position)
        #the page contains PMCID: PMC1787297 in its <dd> element: get any of them that get the text "PMCID" in it
        #find any <dd> element that contains the text "PMCID" in it
        pmcid0=soup.find_all("dd")[0]
        pmcid1=soup.find_all("dd")[1]
        pmcid2=soup.find_all("dd")[2]
        pmcid3=soup.find_all("dd")[2]
        pmc_candidates=[pmcid0,pmcid1,pmcid2,pmcid3]
        #check that pmcid contains the text "PMC"
        pmc=False
        for pmcid in pmc_candidates:
            if "PMC" in pmcid.text:
                pmc=pmcid.text[3:]
        if pmc==False:
            print("Error: pmc not found in page")
            sys.exit()
        write_temp_ref(pmc)
        ref_file = open("temp", "r")
        pdf_type = "jm"
        print("pdf_type: "+pdf_type)
    ref_file.close()
    if cover_page== True:
        pdf_type="cp"
        position=None
    #in the ref file get the PMID reference
    #get the IP and the PMID
    #get the PMID
    #reopen the ref_file
    ref_file = open("temp", "r")

    for line in ref_file:
            #get the issue
        if line.startswith('IP '):
            issue = clean_space(line.split("  -")[-1])
            print("issue: "+issue)
            #get the volume
        if line.startswith('VI'):
            vol = clean_space(line.split("  -")[-1])
            #make sure the volume is 3 digit for instance: 001 not 1
            vol = vol.zfill(3)
        if line.startswith('DP'):
            string_date = line.split("  -")[-1]
            date=parser.parse(string_date)
            year = date.year
            month = str(date.month).zfill(2)
            if clean_space(string_date)[-1].isalpha():
                day=0
            else:    
                day = date.day
            day=str(day).zfill(2)
    print(Fore.GREEN + str(day) +" "+ str(month) +" "+ str(year))
    print(Fore.RESET)
    return {"vol":vol, "year":year, "month":month, "day":day, "issue":issue,"pdf_type":pdf_type}

def get_filename(data,parent_page,position="001"):

    #check if the issue is "suppl":
    if data["issue"]=="Suppl":
        filename= "BMJ_"+str(data["year"])+"_"+str(data["month"])+"_"+str(data["day"])+"_vol"+str(data["vol"])+"_Suppl"
    else:
        filename= "BMJ_"+str(data["year"])+"_"+str(data["month"])+"_"+str(data["day"])+"_vol"+str(data["vol"])+"_nr"+str(data["issue"])
    #remove all spaces and newlines from filename
    filename = filename.replace(" ", "").replace("\n", "")+"_"+data["pdf_type"]+position
    print(filename)
    #print(filename)
    return filename

def file_exists(pmc):
    #open the folder "PDF"
    #for each file in the folder
    #if the pmc is in the filename
    #return True
    #if the pmc is not in the filename
    #return False
    for file in os.listdir("PDF"):
        if pmc in file:
            print(file)
            return True
    return False

def is_time_range(htext):
    """check if the date is in the time range"""
    date=parser.parse(htext)
    if date.year>=startDate and date.year<=endDate:
        return True
    else:
        print("date is not in the time range")
"""
First run: saving the master page with the table to an html file
... because we are nice to the servers.
"""
#document = urllib.request.urlopen(baseUrl)
#soup = BeautifulSoup(document, "lxml")#Uncomment the first time
#open the website Basepage.html parse it with soup
document = open("Basepage.html",'r')
soup = BeautifulSoup(document , "lxml")
document.close()

#document = urllib.request.urlopen(page_link)#uncomment first time it is run
""" print("now saving the parent page")
with open("temp_parent", 'wb') as f:#uncomment when test finished
    #write parent page content
    print(page_link)
    parent = urllib.request.urlopen(page_link)
    print("test")
    #write content of parent
    f.write(parent.read())
    f.close() """
#page_link_links=open("temp_parent",'r')
#soup = BeautifulSoup(page_link_links, "lxml")#Uncomment the first time
#page_link_links=soup.find_all("a")
def download_issue(issue_link):
    i=1
    jm_i=1
    
    #open temp parent and get all the a in it
    f=open("temp_parent",'r')# here an example of issue page also called "parent page": https://www.ncbi.nlm.nih.gov/pmc/issues/167479/
    soup = BeautifulSoup(f, "lxml")#Uncomment the first time
    f.close()
    parent_page=soup.find_all("a")
    #find the number of times the word "Summary" is in the page
    num_summary=len(soup.find_all("a", text="Summary"))
    for page_link in parent_page:
        
        artnr=str(i).zfill(3)
        #a. Get each link to pdf
        #get all links and if the htext contains "PDF–" download the pdf   
        if "PDF–" in page_link.text:
            
            print("loop nr"+str(i))
            print("Found a PDF!")
            
            pdf_link = "https://www.ncbi.nlm.nih.gov"+page_link['href']
            #a reference file can be downloaded with links like this: 
            #https://www.ncbi.nlm.nih.gov/pmc/resources/citations/56681/export/
            #get the pmc number out of the link.text: /pmc/articles/PMC56681/pdf/178p.pdf
            pmc = page_link['href'].split("/")[3][3:]
            if file_exists(pmc):
                print("file exists")
                if "Cover page" in page_link.text:
                    continue
                elif i<=num_summary:
                    i+=1
                    continue
                else:
                    continue

                
                print("ALERT this SHOULD NOT BE")
            else: 
                print("file does not exist")
                print("this is the PMC "+pmc)
                #time.sleep(randint(0,1))
            print("217 issue Link: " + issue_link)
            print("218:pdf_link: "+pdf_link)
            if "Cover page" in page_link.text:
                file_name=get_filename(get_data(pmc,page_link,cover_page=True),page_link,position="000")
            else:
                print("geting the art data")
                article_data=get_data(pmc,page_link,cover_page=False)
                print("got art data")
                if article_data["pdf_type"]=="jm":
                    file_name=get_filename(article_data,page_link,position=str(jm_i).zfill(3))
                    jm_i+=1
                else:
                    file_name=get_filename(article_data,page_link,position=artnr)
                    i+=1

            #reconstitute the link
            ref_link = "https://www.ncbi.nlm.nih.gov/pmc/resources/citations/"+pmc+"/export/"
            print("252 " + ref_link)
            temp_file = open("temp", "r")
            #XX. Write the ref file with the links and the proper name
            ref_file = open("PDF/"+file_name+"_pmc"+pmc+".nbib", "w")
            #add to the end of the ref file the ref link
            #add the link to the pdf at the end of the ref file
            ref_file.write(temp_file.read())
            ref_file.write("PDFLink  - "+pdf_link)
            ref_file.write("\n")
            ref_file.write("RefLink  - "+ref_link)
            ref_file.write("\n")
            ref_file.write("ParentPage  - "+issue_link)
            ref_file.write("\n")
            ref_file.close()
            #XXI Download the pdf file with appropriate name
            r = requests.get(pdf_link, headers=headers)
            with open("PDF/"+file_name+"_pmc"+pmc+".pdf", 'wb') as f:
                f.write(r.content)

if __name__ == '__main__':
    #I. Find the links to the issue pages
#Find all the links of class "arc-issue"
    master_links = soup.find_all("a", class_="arc-issue")
    for link_thingy in master_links:
        for text in link_thingy.find_all('br'):
            if is_time_range(text.next_sibling):
                print(text.next_sibling)                
                issue_link = link_thingy['href']
                print(issue_link)
                #save issue page to a file parent page "temp_parent"
                #open the website page_link parse it with soup
                page = urllib.request.urlopen(issue_link)
                with open("temp_parent", 'wb') as f:#uncomment when test finished
                    #write parent page content
                    f.write(page.read())
                    f.close()
                    download_issue(issue_link)
                    #the temp_parent will be used in a function for taking data
            else:
                print("not in time range")
                break
        

